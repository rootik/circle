import React from 'react';
import RenderDOM from 'react-dom';
import Router from 'react-router';
import Dispather from 'flux-dispatcher';
import EventEmitter from 'event-emitter';

export class baseController {
  constructor() {
    require("../views/styles/style.scss");
    global.React = React;
    global.RenderDOM = RenderDOM;
    global.Model = require("../classes/model");
    global.Store = require("../classes/store");
    global.render = require("../classes/render").default;
    global.THREE = require("../lib/three.min.js");

    render.render();
  }
}

let base = new baseController();
export default base;  
export class Map{
    constructor() {
        this.width = window.innerWidth;
        this.height = window.innerHeight;
        this.canvas = document.getElementById('canvas');
        this.canvas.height = this.height;
        this.canvas.width = this.width;
        this.position = {
            x:0,
            y:100,
            z:1000
        }
    }

    render(){
        let renderer = new THREE.WebGLRenderer({canvas:this.canvas});
        renderer.setClearColor(0x000000);

        return renderer || undefined;
    }

    camera(){
        var camera = new THREE.PerspectiveCamera(45, this.width/this.height, 0.1, 5000);
        camera.position.set(this.position.x,this.position.y,this.position.z);

        return camera || undefined;
    }

    scene() {
        let scene = new THREE.Scene();
        var light = new THREE.AmbientLight(0xaaaaaa);
        scene.add(light);
        var geometry = new THREE.SphereGeometry(200,12,12);
        var material = new THREE.MeshBasicMaterial({color:0x00ff00, wireframe:true});
        var mesh = new THREE.Mesh(geometry, material);
        scene.add(mesh);

        return scene;
    }

    start() {
        var render = this.render();
        var camera = this.camera();
        var scene = this.scene();

        render.render(scene, camera);
    }
}
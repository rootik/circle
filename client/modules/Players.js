import { Map } from './Map';

export class Players extends Map{
    constructor(){
        super();
        this.controller();
    }

    controller() {
        document.addEventListener("keydown", (e)=>{
            if(e.keyCode==87) {
                this.position.z = this.position.z - 3;
            }
            if(e.keyCode==83) {
                this.position.z = this.position.z + 3;
            }

            if (e.keyCode==68) {
                this.position.x = this.position.x + 3;
            }

            if (e.keyCode==65) {
                this.position.x = this.position.x - 3;
            }

            requestAnimationFrame(()=> this.start());
        });

        document.addEventListener("mousemove", (e)=>{
            console.log(e.clientX);
            console.log(e.clientY);
        });

        this.start();
    }
}
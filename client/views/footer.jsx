import React from 'react';

export default class Footer extends React.Component {
    render() {
        return (
            <footer>
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 text-lg-center footer_inner">Its my app</div>
                    </div>
                </div>
            </footer>
        );
    }
}
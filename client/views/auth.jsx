import FloatingActionButton from 'material-ui/FloatingActionButton';
import FontIcon from 'material-ui/FontIcon';
import baseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import Games from '../modules/Games';

class Auth extends Model {
    constructor() {
        super({
            models: ['Users'],
            stores: ['Users']
        });
        this.state = {};
        this.store = this.store['Users'];
        this.model = this.model['Users'];

        this.store.on("change", ()=>{
            this.setState({
                link:this.store._get()[1]
            });
        });
        var games = new Games();
    }

    getChildContext() {
        return { muiTheme: getMuiTheme(baseTheme) };
    }

    componentWillMount(){
        this.model.getLink();
    }

    render() {
        return (
            <div className="container">
                
            </div>
        );
    }
}

Auth.childContextTypes = {
    muiTheme: React.PropTypes.object.isRequired,
};

module.exports = Auth;
import baseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import ListUsers from './list_users';
import RaisedButton from 'material-ui/RaisedButton';

class Games extends Model{
    constructor() {
        super({
            models: ['Room', 'Games'],
            stores: ['Room']
        });


        global.state = {};
        this.modelRoom = this.model['Room'];
        this.modelGames = this.model['Games'];
        this.store = this.store['Room'];

        this.store.on("change", () => {
            state = this.store._get()[0];
            this.setState({
                data:this.store._get()[0]
            });
        });
        
        this.modelRoom.getUsers();
    }

    getChildContext() {
        return { muiTheme: getMuiTheme(baseTheme) };
    }

    getUsers() {
        let users = state;
        var block = [];

        for (var prop in users) {
            if(users.hasOwnProperty(prop)) {
                block[prop] = <div className='col-xs-4'>
                            <div className='row'>
                                <div className="users_block">
                                    <div className='col-xs-12 image'>
                                        <img src={users[prop].photo}/>
                                    </div>
                                    <div className="col-xs-12 name">
                                        {users[prop].email}
                                    </div>
                                </div>
                            </div>
                        </div>;
            }
        }

        return (
            <div className="test">
                {block}
            </div>
        );
    }

    render(){
        this.modelGames.startGameRoom();

        return (
            <main>
                <div className="container" id='mainContainer'>
                    <div className="row">
                        <div className="col-xs-12 text-lg-center">
                            <h3 className='text-lg-center'>Draw a perfect circle</h3>
                            <div className="clock" id="clock">
                                <div>00</div>
                                <div>:</div>
                                <div id="seconds">15</div>
                            </div>
                            <div className="drawPlayers" id="drawPlayers">
                                draw rootikaleks@gmail.com ...
                            </div>
                            <canvas id="myCanvas" width="150px" height="150px"></canvas>
                            <div className='listUsers'>
                                {this.getUsers()}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="hover" id="hover">
                    <div className="hover_hover"></div>
                    <div className="resultGames">
                        <div className="blockResultGames">
                            <div className="list_players">
                                <ListUsers />
                            </div>
                            <RaisedButton label='exit' primary={true} id='closeGame'/>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

Games.childContextTypes = {
    muiTheme: React.PropTypes.object.isRequired,
};

module.exports = Games;
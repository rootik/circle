class Users extends Model {
    constructor() {
        super();
        global.http = this.http;
    }

    getData() {
        if(!localStorage.getItem("get_data")) {
            this.http({
                url:"/users/getDataUsers"
            }).then(result =>{
                if(result) {
                    localStorage.setItem("get_data", result);

                    result = JSON.parse(result);
                    dispatcher.dispatch({type:"GET_DATA", item:result});
                }
            });
        }else{
            setTimeout(()=>{
                dispatcher.dispatch({type:"GET_DATA", item:JSON.parse(localStorage.getItem("get_data"))});
            },50);
        }
    }

    getLink(){
        this.http({
            url:"/users/getLinkGoogle"
        }).then(result=>{
            result = JSON.parse(result);
            dispatcher.dispatch({type:"GET_LINK", item:result.link});
        });
    }

    logout() {
        this.http({
            url:"/users/logout"
        }).then(result=>{
            for (var prop in localStorage) {
                localStorage.removeItem(prop);
            }
            render.render();
        });
    }
}

const users = new Users;
export default users;
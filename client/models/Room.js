class Room extends Model {
    constructor() {
        super();
    }

    getUsers(){
        var users, new_users = {};

        this.http({
            url:"/games/getUserGame?"+localStorage.getItem("id_room")+""
        }).then(result=>{
            result = JSON.parse(result);
            users = result.users;
            new_users = {};

            for (var prop in users) {
                if(users.hasOwnProperty(prop)) {
                    new_users[prop] = users[prop];
                }
            }

            if(result.res==true) {
                dispatcher.dispatch({type:"SET_USERS", item:new_users});
            }
        });
    }
}

const room = new Room;
export default room;
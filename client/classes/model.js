import React from 'react';

class Model extends React.Component {
    // @model array: and array of modals
    constructor(model) {
        super();
        if(model!=undefined){
            this.store = {};
            this.model = {};

            for (var propModels in model.models) { 
                if (model.models.hasOwnProperty(propModels))
                    this.model[model.models[propModels]] = require("../models/"+model.models[propModels]+"").default;
            }
            for (var propStores in model.stores) {
                if(model.stores.hasOwnProperty(propStores)) {
                    this.store[model.stores[propStores]] = require("../stores/"+model.stores[propStores]+"Store");
                }
            }
        }
    }

    http(params) {
        return new Promise((resolve,reject)=>{
            if(params.url) {
                var xhr = new XMLHttpRequest();
                
                if(!params.type) params.type = "GET";

                if(params.type == "GET" && params.data) params.data = "";

                xhr.open(params.type, params.url);
                xhr.send();

                xhr.onreadystatechange = ()=>{
                    if (xhr.readyState!=4) return;

                    xhr.status!=200 ? reject(xhr.statusText + xhr.status) : resolve(xhr.responseText);
                }
            }
        });
    }
}

module.exports = Model;
import React from 'react';
import baseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';


import Header from './header';
import Footer from './footer';
import Main from './main';

// App component - represents the whole app
class App extends React.Component {
  getChildContext() {
      return { muiTheme: getMuiTheme(baseTheme) };
  }

  getFooter(){ var footer = new Footer(); return footer.render(); }
  getMain() { var main = new Main(); return main.render(); }

  render() {
    return (
      <div id="wrapper">
        <div className="flex">
          <Header />
          <Main />
          <Footer />
        </div>
      </div>
    );
  }
}

App.childContextTypes = {
    muiTheme: React.PropTypes.object.isRequired,
};

module.exports = App;